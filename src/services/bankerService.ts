import { redisStorage } from "../redis/redisStorage";
import { campaign } from "../models/campaign";
import { auction } from "../models/auction";
import { timingSafeEqual } from "crypto";

export class bankerService {
    numberOfRequestsPerSec = 10000;
    numberOfServices = 5;
    campaignBudgetFor15Sec = this.numberOfRequestsPerSec/this.numberOfServices*15;
    redis: redisStorage = new redisStorage();
    campaigns: campaign[] = [ 
        {campaignId:1,
        campaignBudget: 0},
        {campaignId:2,
        campaignBudget:0},
        {campaignId:3,
        campaignBudget:0}
    ];
    auctions: auction[];
    constructor() {
        setInterval(() => {
            this.campaigns.forEach(async (c) => {
                let FullCampaignBudget = await this.redis.decreaseValue(c.campaignId, this.campaignBudgetFor15Sec);
                if (FullCampaignBudget) {
                    if (FullCampaignBudget < 0) {
                        await this.redis.increaseValue(c.campaignId, this.campaignBudgetFor15Sec);
                    }
                    else {
                        c.campaignBudget += this.campaignBudgetFor15Sec;
                    }
                }
            })
        },10000);
    }

    public auctionForCampaign(campaignId:number, price:number, auctionId) {
        let campaign = this.campaigns.find(c => c.campaignId == campaignId);

        if (!campaign) {
            return false;
        }
        else {
            campaign.campaignBudget -= price;

            if (campaign.campaignBudget < 0) {
                campaign.campaignBudget += price;
                return false;
            }
            else {
                this.redis.set(auctionId, { price: price, campaignId: campaign.campaignId });
            } 

            return true;
        }
    }

    public handleAuctionResult(auctionId: number, actionSucceed: boolean) {
        if (!actionSucceed) {
            this.increaseUnusedBudget(auctionId)
        }
    }

    private async increaseUnusedBudget(auctionId: number) {
        let auctionString = await this.redis.get(auctionId);
        let auctionData = JSON.parse(auctionString);
        let campaign = this.campaigns.find(c => c.campaignId == auctionData.campaignId);
        campaign.campaignBudget += auctionData.price;
    }
}
