export interface auction {
    auctionId: number,
    price: number,
    campaignId: number
}