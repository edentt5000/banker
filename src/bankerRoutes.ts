import express from 'express';
import { bankerService } from './services/bankerService';
const router = express.Router();

router.get('/', (req, res) => {
    res.json('test');
  })

router.put('/campaignBudget', (req, res) => {
  let banker = new bankerService();
  res.json(banker.auctionForCampaign(req.body.campaignId, req.body.price, req.body.auctionId));
})

router.put('/auctionResult', async (req, res) => {
    let banker = new bankerService();
    await banker.handleAuctionResult(req.body.auctionId, req.body.actionSucceed);
    res.send(200);
})

export default router;
