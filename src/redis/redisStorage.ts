import { promisify } from 'util';
const redis = require("redis");


export class redisStorage {
    redisClient: any;
    constructor() {
        this.redisClient = redis.createClient();
    }

    public async get(key) {
        const getData = promisify(this.redisClient.get).bind(this.redisClient);
        return await getData(key);
    }

    public set(key, value) {
        const setData = promisify(this.redisClient.set).bind(this.redisClient);
        return setData(key, JSON.stringify(value));
    }

    public async increaseValue(key, amount) {
        const incrData = promisify(this.redisClient.incrby).bind(this.redisClient);
        return await incrData(key, amount);
    }

    public async decreaseValue(key, amount) {
        const decrData = promisify(this.redisClient.decrby).bind(this.redisClient);
        return await decrData(key, amount);
    }
}
